import React from "react";

//const data = require("./test.json");

class Nav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      links: [],
      //links: data.navLinks,
      hidden: true,
      linksClass: "navHidden",
      buttonClass: "fa fa-bars",
    };
    this.toggleNav = this.toggleNav.bind(this);
  }

  // Get navLinks from api
  componentDidMount() {
    fetch("/api/navlinks")
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          links: data.navLinks,
        });
      });
  }

  toggleNav(e) {
    if (this.state.hidden) {
      this.setState({
        hidden: false,
        linksClass: "",
        buttonClass: "fa fa-times",
      });
    } else {
      this.setState({
        hidden: true,
        linksClass: "navHidden",
        buttonClass: "fa fa-bars",
      });
    }
  }

  render() {
    return (
      <header id="nav">
        <nav className="nav-box">
          <div className="nav-header">
            <a href="#">
              <h2> brandon clyde</h2>
            </a>
          </div>
          <div className="nav-button">
            <button
              className={this.state.buttonClass}
              onClick={this.toggleNav}
            />
          </div>
          <div className={"nav-links " + this.state.linksClass}>
            {this.state.links.map((link) => {
              return <NavLink data={link} />;
            })}
          </div>
        </nav>
      </header>
    );
  }
}

class NavLink extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      link: this.props.data.link,
      text: this.props.data.text,
    };
  }

  render() {
    return (
      <div className="nav-link">
        <a href={this.state.link}>
          <h3>{this.state.text}</h3>
        </a>
      </div>
    );
  }
}

export default Nav;
