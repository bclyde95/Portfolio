import React from "react";

class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Brandon Clyde",
      year: new Date().getFullYear(),
    };
  }

  render() {
    return (
      <section id="footer">
        <div className="copyright">
          <h3>
            &copy; {this.state.name} {this.state.year}
          </h3>
        </div>
      </section>
    );
  }
}

export default Footer;
