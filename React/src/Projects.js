import React from "react";

//const data = require('./test.json');

class Projects extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
      //projects: data.projects
    };
  }

  // Get projects from api
  componentDidMount() {
    fetch("/api/projects")
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          projects: data.projects,
        });
      });
  }

  render() {
    return (
      <section id="projects" className="content">
        <h2>Portfolio</h2>
        <div className="project-box">
          {/* Build project tiles from objects in json response */}
          {this.state.projects.map((project) => {
            return <Project data={project} />;
          })}
        </div>
      </section>
    );
  }
}

class Project extends React.Component {
  constructor(props) {
    super(props);
    // init state with input data
    this.state = {
      link: this.props.data.link,
      title: this.props.data.title,
      desc: this.props.data.desc,
      technologies: this.props.data.technologies,
      content: (
        <div className="project-content">
          <h3>{this.props.data.title}</h3>
        </div>
      ),
      flipped: false,
      style: {},
    };
  }

  render() {
    // function to change tile to detail view
    const flipTile = () => {
      if (this.state.flipped == false) {
        this.setState({
          // set content to more complex div
          content: (
            <div className="project-content-toggled">
              {/**** Project Info *****/}
              <h3>{this.state.title}</h3>
              <p>{this.state.desc}</p>
              {/**** Technologies *****/}
              <h4>Technologies Used</h4>
              <ul>
                {/* Build technology list from list in state */}
                {this.state.technologies.map((tech) => {
                  return <li className="project-tech">{tech}</li>;
                })}
              </ul>
              {/**** Button *****/}
              <div className="project-btn-box">
                <a href={this.state.link} target="_blank">
                  <button className="project-btn">See Project</button>
                </a>
              </div>
            </div>
          ),
          flipped: true,
          style: {
            backgroundColor: "#fff",
            color: "#344f5e",
          },
        });
      } else {
        this.setState({
          content: (
            <div className="project-content">
              {/**** Project Title ****/}
              <h3>{this.props.data.title}</h3>
            </div>
          ),
          flipped: false,
          style: {},
        });
      }
    };

    return (
      <div className="project-tile" onClick={flipTile} style={this.state.style}>
        {this.state.content}
      </div>
    );
  }
}

export default Projects;
