import React from "react";
import ReactDOM from "react-dom";
import Nav from "./Nav.js";
import Landing from "./Landing.js";
import Projects from "./Projects.js";
import Contact from "./Contact.js";
import Footer from "./Footer.js";

class App extends React.Component {
  render() {
    return (
      <div id="app">
        <Nav />
        <Landing />
        <Projects />
        <Contact />
        <Footer />
      </div>
    );
  }
}

export default App;
