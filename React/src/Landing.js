import React from "react";

class Landing extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section id="landing" className="content">
        <div id="landing-box">
          <h1>
            {/* Place title sections in flexbox to stack for small screen readibility */}
            <div className="title-box">
              {/* Wrap title pieces in template literals to avoid conflict with JSX */}
              <span>{`{ Software Engineering }`}</span>
              <span>{`< Web Development />`}</span>
            </div>
          </h1>
          <h2>
            Software engineer with proficiency in Python, C, and Web
            Technologies
          </h2>
          <h2>
            Electrical Engineering Student at Oregon Institute of Technology
          </h2>
          <a href="#contact">
            <button id="landing-btn">Contact for a Quote</button>
          </a>
        </div>
      </section>
    );
  }
}

export default Landing;
