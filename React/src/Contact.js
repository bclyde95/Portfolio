import React from "react";

//const data = require("./test.json")

class Contact extends React.Component {
  render() {
    return (
      <section id="contact" className="content">
        <h2>Contact</h2>
        <hr />
        <div className="contact-box">
          <ContactForm />
          <ContactLinks />
        </div>
      </section>
    );
  }
}

class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      subject: "",
      message: "",
      sent: "",
      sentStyle: { display: "none" },
    };
    // bind 'this'
    this.onInput = this.onInput.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.sentOnClick = this.sentOnClick.bind(this);
  }

  // Update text inside of input as user types. Uses input name as state key
  onInput(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  // Save state to output variable, stop the page from refreshing, and empty state.
  onSubmit(e) {
    e.preventDefault();

    // Build request
    const output = {
      name: this.state.name,
      email: this.state.email,
      subject: this.state.subject,
      message: this.state.message,
    };
    const options = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(output),
    };

    // send message to contact endpoint
    fetch("/api/contact", options)
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          sent: data.message,
        });
      });

    this.setState({
      name: "",
      email: "",
      subject: "",
      message: "",
      sentStyle: {},
    });
  }

  sentOnClick(e) {
    this.setState({
      sentStyle: { display: "none" },
    });
  }

  render() {
    return (
      // set onSubmit to handler function
      <form onSubmit={this.onSubmit} className="contact-form">
        <fieldset>
          <legend>Send a Message</legend>
          <p
            className="contact-sent"
            style={this.state.sentStyle}
            onClick={this.sentOnClick}
          >
            {this.state.sent}
          </p>
          <div className="contact-input">
            <input
              name="name"
              value={this.state.name}
              placeholder="Name"
              onChange={this.onInput}
              type="text"
              required
            />
          </div>
          <div className="contact-input">
            <input
              name="email"
              value={this.state.email}
              placeholder="Email"
              onChange={this.onInput}
              type="email"
              required
            />
          </div>
          <div className="contact-input">
            <input
              name="subject"
              value={this.state.subject}
              placeholder="Subject"
              onChange={this.onInput}
              type="text"
              required
            />
          </div>
          <div className="contact-input">
            <textarea
              name="message"
              value={this.state.message}
              placeholder="Message"
              onChange={this.onInput}
            />
          </div>
          <div className="contact-btn">
            <button type="submit">Send</button>
          </div>
        </fieldset>
      </form>
    );
  }
}

class ContactLinks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      links: [],
      //links: this.props.data.socialLinks
    };
  }

  // Get socialLinks from api
  componentDidMount() {
    fetch("/api/sociallinks")
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          links: data.socialLinks,
        });
      });
  }

  render() {
    return (
      <div className="contact-links">
        {this.state.links.map((link) => {
          return <ContactLink data={link} />;
        })}
      </div>
    );
  }
}

class ContactLink extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      link: this.props.data.link,
      icon: this.props.data.icon,
      alt: this.props.data.alt,
    };
  }

  render() {
    return (
      <a href={this.state.link} className="contact-link" target="_blank">
        <i className={"fab " + this.state.icon} alt={this.state.alt}></i>
      </a>
    );
  }
}

export default Contact;
