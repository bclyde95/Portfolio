"""
SocialLink.py:
    Data class and orm binding for social links
"""

# import db
from .Shared import db


class SocialLink(db.Model):
    """ Defines social link attributes """
    __tablename__ = 'sociallinks'

    id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.VARCHAR(50), nullable=False)
    icon = db.Column(db.VARCHAR(50), nullable=False)
    alt = db.Column(db.VARCHAR(50), nullable=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion"""
        return dict(
            id=self.id,
            link=self.link,
            icon=self.icon,
            alt=self.alt
        )

    @classmethod
    def getById(cls, id):
        """ returns SocialLink if id of the class is in the database. Null if not """
        return cls.query.filter(cls.id == id).first()

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()

    def deletefromDb(self):
        db.session.delete(self)
        db.session.commit()
