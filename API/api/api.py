"""
api.py:
    routing and api CRUD functions
"""

# library imports
from flask import Blueprint, jsonify, request
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity

# file imports
from .Shared import db, checkPassword, jsonError
from .Admin import Admin

from .NavLink import NavLink
from .Project import Project, Technology
from .Message import Message
from .SocialLink import SocialLink

api = Blueprint('api', __name__)


###---- Data Routes ----###

""" Endpoint to get nav links. Client route is GET only """


@api.route('/navlinks', methods=['GET'])
def navLinks():
    """ Gets all navlinks from db, then returns them in JSON response """
    try:
        navlinks = NavLink.query.all()

        # return list of nav links, error if failure
        return jsonify({
            "status": "success",
            "navLinks": [link.toDict() for link in navlinks]
        }), 200
    except Exception:
        return jsonError('Something went wrong', 500)


""" Endpoint to get projects. Client route is GET only """


@api.route('/projects', methods=['GET'])
def portfolio():
    """ Gets all projects from db, then returns them in JSON response """
    try:
        projects = Project.query.all()

        # return list of projects, error if failure
        return jsonify({
            "status": "success",
            "projects": [project.toDict() for project in projects]
        }), 200
    except Exception:
        return jsonError('Something went wrong', 500)


""" Endpoint to store messages from contact form. Client route is POST only """


@api.route('/contact', methods=['POST'])
def message():
    """ Takes in POST request with message data and stores into db """
    data = request.get_json()

    # make sure request is not missing properties
    if "name" in data:
        if "email" in data:
            if "subject" in data:
                if "message" in data:
                    # create new Message
                    newMessage = Message(
                        name=data["name"],
                        email=data["email"],
                        subject=data["subject"],
                        message=data["message"]
                    )

                    # Commit new message to db. If successful, return success message. If not, return error
                    try:
                        newMessage.commitToDb()
                        return jsonify({
                            "status": "success",
                            'message': "Message successfully sent"
                        }), 201
                    except Exception:
                        return jsonError('Error occured. Message not sent!', 500)
                else:
                    return jsonError('missing "message" property', 404)
            else:
                return jsonError('missing "subject" property', 404)
        else:
            return jsonError('missing "email" property', 404)
    else:
        return jsonError('missing "name" property', 404)


""" Endpoint to get social links. Client route is GET only """


@api.route('/sociallinks', methods=['GET'])
def sociallinks():
    """ Gets all social links from db, then returns them in JSON response """
    try:
        socialLinks = SocialLink.query.all()

        # return list of social links, error if failure
        return jsonify({
            "status": "success",
            "socialLinks": [link.toDict() for link in socialLinks]
        }), 200
    except Exception:
        return jsonError('Something went wrong', 500)

###----------------------###


###---- Admin Routes ----###

""" Endpoint to create/update/delete navlinks. Must have access token """


@api.route('/admin/navlinks/<string:action>', methods=['POST'])
@jwt_required(refresh=False)
def navLinksAdmin(action=""):
    """ Takes in a POST request with action type and data dict """
    data = request.get_json()

    if action == "create":
        if "link" in data:
            if "text" in data:
                newNavLink = NavLink(
                    link=data["link"],
                    text=data["text"]
                )

                # Commit new nav link to db. If successful, return success message. If not, return error
                try:
                    newNavLink.commitToDb()
                    return jsonify({
                        "status": "success",
                        'message': "Nav link created"
                    }), 201
                except Exception:
                    return jsonError('Something went wrong', 500)
            else:
                return jsonError('missing "text" property', 404)
        else:
            return jsonError('missing "link" property', 404)
    elif action == "update":
        updated = []
        if "id" in data:
            navLink = NavLink.getById(data["id"])
            if navLink:
                if "link" in data:
                    navLink.link = data["link"]
                    updated.append("link")
                if "text" in data:
                    navLink.text = data["text"]
                    updated.append("text")

                # try to commit to db, error if failed
                try:
                    navLink.commitToDb()
                    return jsonify({
                        'status': 'success',
                        'message': 'Changes saved for {0}'.format(', '.join(updated))
                    }), 200
                except Exception:
                    return jsonError('Something went wrong', 500)
            else:
                return jsonError('NavLink does not exist', 404)
        else:
            return jsonError('missing "id" property', 404)

    elif action == "delete":
        if "id" in data:
            navLink = NavLink.getById(data["id"])
            if navLink:
                # try to delete from db, error if failed
                try:
                    navLink.deletefromDb()
                    return jsonify({
                        'status': 'success',
                        'message': 'Nav link {0} deleted'.format(data["id"])
                    }), 200
                except Exception:
                    return jsonError('Something went wrong', 500)
            else:
                return jsonError('NavLink does not exist', 404)
        else:
            return jsonError('missing "id" property', 404)
    else:
        return jsonError('Invalid action. Must be CREATE, UPDATE, or DELETE', 422)


""" Endpoint to create/update/delete projects. Must have access token """


@api.route('/admin/projects/<string:action>', methods=['POST'])
@jwt_required(refresh=False)
def projectsAdmin(action=""):
    """ Takes in a POST request with action type and data dict """
    data = request.get_json()

    if action == "create":
        if "link" in data:
            if "title" in data:
                if "desc" in data:
                    if "technologies" in data:
                        newProject = Project(
                            link=data["link"],
                            title=data["title"],
                            desc=data["desc"],
                            technologies=[]
                        )

                        for technology in data["technologies"]:
                            newProject.technologies.append(Technology(
                                projectId=newProject.id, name=technology))

                        # Commit new nav link to db. If successful, return success message. If not, return error
                        try:
                            newProject.commitToDb()
                            return jsonify({
                                "status": "success",
                                'message': "Project created"
                            }), 201
                        except Exception:
                            return jsonError('Something went wrong', 500)

                    else:
                        return jsonError('missing "technologies" property', 404)
                else:
                    return jsonError('missing "desc" property', 404)
            else:
                return jsonError('missing "title" property', 404)
        else:
            return jsonError('missing "link" property', 404)
    elif action == "update":
        updated = []
        if "id" in data:
            project = Project.getById(data["id"])
            if project:
                if "link" in data:
                    project.link = data["link"]
                    updated.append("link")
                if "title" in data:
                    project.title = data["title"]
                    updated.append("title")
                if "desc" in data:
                    project.desc = data["desc"]
                    updated.append("desc")
                if "technologies" in data:
                    for technology in data["technologies"]:
                        if technology not in [tech.name for tech in project.technologies]:
                            project.technologies.append(Technology(
                                projectId=project.id, name=technology))
                            updated.append("technologies")

                # try to commit to db, error if failed
                try:
                    project.commitToDb()
                    return jsonify({
                        'status': 'success',
                        'message': 'Changes saved for {0}'.format(', '.join(updated))
                    }), 200
                except Exception:
                    return jsonError('Something went wrong', 500)
            else:
                return jsonError('Project does not exist', 404)
        else:
            return jsonError('missing "id" property', 404)

    elif action == "delete":
        if "id" in data:
            project = Project.getById(data["id"])

            if project:
                # try to delete from db, error if failed
                try:
                    project.deletefromDb()
                    return jsonify({
                        'status': 'success',
                        'message': 'Project {0} deleted'.format(data["id"])
                    }), 200
                except Exception:
                    return jsonError('Something went wrong', 500)
            else:
                return jsonError('Project does not exist', 404)
        else:
            return jsonError('missing "id" property', 404)
    else:
        return jsonError('Invalid action. Must be CREATE, UPDATE, or DELETE', 422)


""" Endpoint to get/delete messages. Must have access token """


@api.route('/admin/contact', methods=['GET', 'POST'])
@jwt_required(refresh=False)
def messageAdmin():
    """ If GET, gets all messages from db; then returns them in JSON response. If POST, deletes message at id """
    if request.method == 'GET':
        try:
            messages = Message.query.all()

            # return list of messages, error if failure
            return jsonify({
                "status": "success",
                "messages": [message.toDict() for message in messages]
            }), 200
        except Exception:
            return jsonError('Something went wrong', 500)
    if request.method == 'POST':
        if "id" in data:
            message = Message.getById(data["id"])

            if message:
                # try to delete from db, error if failed
                try:
                    message.deletefromDb()
                    return jsonify({
                        'status': 'success',
                        'message': 'Message {0} deleted'.format(data["id"])
                    }), 200
                except Exception:
                    return jsonError('Something went wrong', 500)
            else:
                return jsonError('Message does not exist', 404)
        else:
            return jsonError('missing "id" property', 404)


""" Endpoint to create/update/delete sociallinks. Must have access token """


@api.route('/admin/sociallinks/<string:action>', methods=['POST'])
@jwt_required(refresh=False)
def socialLinksAdmin(action=""):
    """ Takes in a POST request with action type and data dict """
    data = request.get_json()

    if action == "create":
        if "link" in data:
            if "icon" in data:
                if "alt" in data:
                    newSocialLink = SocialLink(
                        link=data["link"],
                        icon=data["icon"],
                        alt=data["alt"]
                    )

                    # Commit new nav link to db. If successful, return success message. If not, return error
                    try:
                        newSocialLink.commitToDb()
                        return jsonify({
                            "status": "success",
                            'message': "Social link created"
                        }), 201
                    except Exception:
                        return jsonError('Something went wrong', 500)
                else:
                    return jsonError('missing "alt" property', 404)
            else:
                return jsonError('missing "icon" property', 404)
        else:
            return jsonError('missing "link" property', 404)
    elif action == "update":
        updated = []
        if "id" in data:
            socialLink = SocialLink.getById(data["id"])
            if socialLink:
                if "link" in data:
                    socialLink.link = data["link"]
                    updated.append("link")
                if "icon" in data:
                    socialLink.icon = data["icon"]
                    updated.append("icon")
                if "alt" in data:
                    socialLink.alt = data["alt"]
                    updated.append("alt")

                # try to commit to db, error if failed
                try:
                    socialLink.commitToDb()
                    return jsonify({
                        'status': 'success',
                        'message': 'Changes saved for {0}'.format(', '.join(updated))
                    }), 200
                except Exception:
                    return jsonError('Something went wrong', 500)
            else:
                return jsonError('Social link does not exist', 404)
        else:
            return jsonError('missing "id" property', 404)

    elif action == "delete":
        if "id" in data:
            socialLink = SocialLink.getById(data["id"])

            if socialLink:
                # try to delete from db, error if failed
                try:
                    socialLink.deletefromDb()
                    return jsonify({
                        'status': 'success',
                        'message': 'Social link {0} deleted'.format(data["id"])
                    }), 200
                except Exception:
                    return jsonError('Something went wrong', 500)
            else:
                return jsonError('Social link does not exist', 404)
        else:
            return jsonError('missing "id" property', 404)
    else:
        return jsonError('Invalid action. Must be CREATE, UPDATE, or DELETE', 422)

###--------------###


###---- AUTH ----###

""" Endpoint to login and recieve an access token """


@api.route('/auth/login', methods=['POST'])
def login():
    """ Takes in POST request with login info, veriifies the login, and returns API access and refresh tokens """
    data = request.get_json()

    # Tries to find Admin in database, stores None in admin if not.
    admin = Admin.getByEmail(data["userIn"])

    # if admin is in database, check password. If not, return Admin does not exist error.
    if admin:
        # Checks password with password in db, returns access and refresh token if successful. Error if not.
        if checkPassword(admin.password, data['password']):
            access_token = create_access_token(identity=admin.id)
            refresh_token = create_refresh_token(identity=admin.id)
            return jsonify({
                "status": "success",
                "accessToken": access_token,
                "refreshToken": refresh_token
            }), 200
        else:
            return jsonError('Incorrect password', 401)
    if not admin:
        return jsonError('Admin does not exist', 404)
    else:
        return jsonError('Something went wrong', 500)


@api.route('/auth/tokenrefresh', methods=['POST'])
@jwt_required(refresh=True)
def tokenRefresh():
    """ Takes in refresh token and returns new access token """
    adminId = get_jwt_identity()
    access_token = create_access_token(identity=adminId)

    # try to return access token, if error return error
    try:
        return jsonify({
            'status': 'success',
            'accessToken': access_token
        }), 201
    except Exception:
        return jsonError('Something went wrong', 500)

###---------------------###
