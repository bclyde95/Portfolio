"""
NavLink.py:
    Data class and orm binding for Navigation Links
"""

# import db
from .Shared import db


class NavLink(db.Model):
    """ Defines navigation link attributes """
    __tablename__ = 'navlinks'

    id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.VARCHAR(50), nullable=False)
    text = db.Column(db.VARCHAR(50), nullable=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion"""
        return dict(
            id=self.id,
            link=self.link,
            text=self.text
        )

    @classmethod
    def getById(cls, id):
        """ returns User if email in the class is in the database. Null if not """
        return cls.query.filter(cls.id == id).first()

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()

    def deletefromDb(self):
        db.session.delete(self)
        db.session.commit()
