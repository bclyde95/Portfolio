"""
Shared.py:
    Database init and shared data classes
"""

from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash

# init db
db = SQLAlchemy()


def hashPassword(password):
    return generate_password_hash(password)


def checkPassword(hashed, password):
    return check_password_hash(hashed, password)


def jsonError(message, code):
    return jsonify({
        'status': 'error',
        'message': message
    }), code
