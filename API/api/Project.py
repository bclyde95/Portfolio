"""
Project.py:
    Data class and orm binding for Projects
"""

# import db
from .Shared import db


class Project(db.Model):
    """ Defines Project attributes and links to technologies """
    __tablename__ = 'projects'

    id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.VARCHAR(50), nullable=False)
    title = db.Column(db.VARCHAR(50), nullable=False)
    desc = db.Column(db.VARCHAR(250), nullable=False)
    technologies = db.relationship(
        'Technology', backref='Project', lazy='joined')

    def toDict(self):
        """ Constructs data into JSON-like format for conversion"""
        return dict(
            id=self.id,
            link=self.link,
            title=self.title,
            desc=self.desc,
            technologies=[tech.getValue()
                          for tech in getattr(self, "technologies")]
        )

    @classmethod
    def getById(cls, id):
        """ returns Project if id of the class is in the database. Null if not """
        return cls.query.filter(cls.id == id).first()

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()

    def deletefromDb(self):
        db.session.delete(self)
        db.session.commit()


class Technology(db.Model):
    """ Defines Project Technology and links back to Project """
    __tablename__ = 'technologies'

    id = db.Column(db.Integer, primary_key=True)
    projectId = db.Column(db.Integer, db.ForeignKey('projects.id'))
    name = db.Column(db.VARCHAR(50))

    def getValue(self):
        """ Returns the technology name """
        return self.name
