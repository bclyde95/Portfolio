"""
Admin.py:
    data class for Admin. Used for editing the data in the API
"""

# import db
from .Shared import db


class Admin(db.Model):
    """ Login credentials """
    __tablename__ = 'admins'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.VARCHAR(25), nullable=False)
    email = db.Column(db.VARCHAR(50), nullable=False)
    password = db.Column(db.String(250), nullable=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion"""
        return dict(
            id=self.id,
            username=self.username,
            email=self.email
        )

    @classmethod
    def getByEmail(cls, email):
        """ returns User if email in the class is in the database. Null if not """
        return cls.query.filter(cls.email == email).first()

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()
