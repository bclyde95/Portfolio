"""
Message.py:
    Data class and orm binding for messages.
"""

# import db
from .Shared import db


class Message(db.Model):
    """ Defines message attributes """
    __tablename__ = 'messages'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.VARCHAR(50), nullable=False)
    email = db.Column(db.VARCHAR(50), nullable=False)
    subject = db.Column(db.VARCHAR(50), nullable=False)
    message = db.Column(db.Text, nullable=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion"""
        return dict(
            id=self.id,
            name=self.name,
            email=self.email,
            subject=self.subject,
            message=self. message
        )

    @classmethod
    def getById(cls, id):
        """ returns User if email in the class is in the database. Null if not """
        return cls.query.filter(cls.id == id).first()

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()
