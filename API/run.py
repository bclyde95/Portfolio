"""
run.py:
    runs the api in the dev server
"""

if __name__ == "__main__":
    from app import app
    app.run()
